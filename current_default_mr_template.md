## What does this MR do?

<!--

Describe in detail what your merge request does and why.

Are there any risks involved with the proposed change? What additional
test coverage is introduced to offset the risk?

Please keep this description up-to-date with any discussion that takes
place so that reviewers can understand your intent. This is especially
important if they didn't participate in the discussion.

-->

## Screenshots or Screencasts (strongly suggested)

<!-- 

Please include any relevant screenshots that will assist reviewers and
future readers. If you need help visually verifying the change, please 
leave a comment and ping a GitLab reviewer, maintainer, or MR coach.

-->

## How to setup and validate locally (strongly suggested)

<!-- 

Example below:
 
1. Enable the invite modal
   ```ruby
   Feature.enable(:invite_members_group_modal)
   ```
1. In rails console enable the experiment fully
   ```ruby
   Feature.enable(:member_areas_of_focus)
   ```
1. Visit any group or project member pages such as `http://127.0.0.1:3000/groups/flightjs/-/group_members`
1. Click `invite members` button.

-->

## Does this MR meet the acceptance criteria?

<!-- Feel free to delete any tasks that are not applicable. -->

### Conformity

- [ ] I have included changelog trailers, or none are needed. ([Does this MR need a changelog?](https://docs.gitlab.com/ee/development/changelog.html#what-warrants-a-changelog-entry))
- [ ] I have added/updated documentation, or it's not needed. ([Is documentation required?](https://about.gitlab.com/handbook/engineering/ux/technical-writing/workflow/#when-documentation-is-required))
- [ ] I have properly separated EE content from FOSS, or this MR is FOSS only. ([Where should EE code go?](https://docs.gitlab.com/ee/development/ee_features.html#separation-of-ee-code))
- [ ] I have added [information for database reviewers in the MR description](https://docs.gitlab.com/ee/development/database_review.html#required), or it's not needed. ([Does this MR have database related changes?](https://docs.gitlab.com/ee/development/database_review.html))
- [ ] I have self-reviewed this MR per [code review guidelines](https://docs.gitlab.com/ee/development/code_review.html).
- [ ] This MR does not harm performance, or I have asked a reviewer to help assess the performance impact. ([Merge request performance guidelines](https://docs.gitlab.com/ee/development/merge_request_performance_guidelines.html))
- [ ] I have followed the [style guides](https://docs.gitlab.com/ee/development/contributing/style_guides.html).
- [ ] This change is [backwards compatible across updates](https://docs.gitlab.com/ee/development/multi_version_compatibility.html), or this does not apply.

### Availability and Testing

<!--

What risks does this change pose? How might it affect the quality/performance of the product?
What additional test coverage or changes to tests will be needed?
Will it require cross-browser testing?
See the test engineering process for further guidelines: https://about.gitlab.com/handbook/engineering/quality/test-engineering/

-->

- [ ] I have added/updated tests following the [Testing Guide](https://docs.gitlab.com/ee/development/testing_guide/index.html), or it's not needed. (Consider [all test levels](https://docs.gitlab.com/ee/development/testing_guide/testing_levels.html). See the [Test Planning Process](https://about.gitlab.com/handbook/engineering/quality/test-engineering).)
- [ ] I have tested this MR in [all supported browsers](https://docs.gitlab.com/ee/install/requirements.html#supported-web-browsers), or it's not needed.
- [ ] I have informed the Infrastructure department of a default or new setting change per [definition of done](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#definition-of-done), or it's not needed.

### Security

Does this MR contain changes to processing or storing of credentials or tokens, authorization and authentication methods or other items described in [the security review guidelines](https://about.gitlab.com/handbook/engineering/security/#when-to-request-a-security-review)? If not, then delete this Security section.

- [ ] Label as ~security and @ mention `@gitlab-com/gl-security/appsec`
- [ ] The MR includes necessary changes to maintain consistency between UI, API, email, or other methods
- [ ] Security reports checked/validated by a reviewer from the AppSec team

